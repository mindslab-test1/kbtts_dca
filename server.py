import os
import grpc
import time
import torch
import logging
import argparse
import threading
import numpy as np
import signal

from concurrent import futures
from omegaconf import OmegaConf
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from datasets.text import Language
from tacotron import Tacotron

import tts_pb2
from tts_pb2_grpc import add_AcousticServicer_to_server, AcousticServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class TacotronImpl(AcousticServicer):
    def __init__(self, checkpoint_path, max_decoder_steps,
                 mel_chunk_size, is_fp16, backup_path):
        super().__init__()
        self.default_config = {
            'max_decoder_steps': str(max_decoder_steps)
        }
        self.lock = threading.Lock()
        self.running_set = set()
        self.waiting_queue = []

        self.backup_path = backup_path
        if backup_path != "":
            if os.path.exists(backup_path):
                logging.info('Load Config File: {}'.format(backup_path))
                backup_config = OmegaConf.load(backup_path)
                checkpoint_path = backup_config.path
                max_decoder_steps = backup_config.max_decoder_steps
            else:
                self._save_config(checkpoint_path, max_decoder_steps)

        self.is_fp16 = is_fp16
        self.mel_chunk_size = mel_chunk_size
        self.model_status = tts_pb2.ModelStatus()

        try:
            self._load_checkpoint(checkpoint_path, max_decoder_steps)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logging.exception(e)

    @torch.no_grad()
    def Txt2Mel(self, in_text, context):
        try:
            mel_generator = self._inference(in_text, context, self.max_decoder_steps)
            if mel_generator is None:
                return tts_pb2.MelSpectrogram()

            mel_outputs_postnet = [mel for mel in mel_generator][0]
            sample_length = mel_outputs_postnet.size(-1) * self.mel_config.hop_length
            mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

            mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet,
                                         sample_length=sample_length)
            return mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @torch.no_grad()
    def StreamTxt2Mel(self, in_text, context):
        try:
            mel_generator = self._inference(in_text, context, self.mel_chunk_size)
            if mel_generator is not None:
                for mel_outputs_postnet in mel_generator:
                    mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

                    mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet)
                    yield mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    def GetMelConfig(self, empty, context):
        if self.model_status.state == tts_pb2.MODEL_STATE_RUNNING:
            return self.mel_config
        else:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return tts_pb2.MelConfig()

    def SetModel(self, in_model, context, is_running=False):
        try:
            del_keys = [
                config_name
                for config_name in in_model.config.keys()
                if config_name not in self.default_config.keys()
            ]
            for del_key in del_keys:
                del in_model.config[del_key]
            for config_name, config_value in self.default_config.items():
                if config_name not in in_model.config.keys() or in_model.config[config_name] == '':
                    in_model.config[config_name] = config_value

            result = None
            waiting_key = None
            with self.lock:
                if 0 < len(self.waiting_queue):
                    waiting_key = threading.get_ident()
                    self.waiting_queue.append(waiting_key)
                else:
                    if in_model == self.model_status.model or in_model.path == "":
                        if is_running:
                            self.running_set.update([threading.get_ident()])
                        result = tts_pb2.SetModelResult(result=True)
                    elif in_model != self.model_status.model:
                        if 0 < len(self.running_set):
                            waiting_key = threading.get_ident()
                            self.waiting_queue.append(waiting_key)
                        else:
                            if is_running:
                                self.running_set.update([threading.get_ident()])
                            result = self._set_model(in_model)

            if waiting_key is not None:
                while True:
                    with self.lock:
                        if self.waiting_queue[0] == waiting_key:
                            if in_model == self.model_status.model or in_model.path == "":
                                if is_running:
                                    self.running_set.update([threading.get_ident()])
                                del self.waiting_queue[0]
                                result = tts_pb2.SetModelResult(result=True)
                                break
                            elif in_model != self.model_status.model:
                                if 0 == len(self.running_set):
                                    if is_running:
                                        self.running_set.update([threading.get_ident()])
                                    del self.waiting_queue[0]
                                    result = self._set_model(in_model)
                                    break
                    time.sleep(0.01)
            return result
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetModel(self, empty, context):
        return self.model_status

    def _inference(self, in_text, context, mel_chunk_size):
        logging.debug('_inference/in_text/%s', in_text)
        result = self.SetModel(in_text.model, context, True)
        if not result.result:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(result.error)
            return None
        if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return None
        if self.max_speaker < in_text.speaker:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("exceeded maxspeaker")
            return None

        text = in_text.text
        text = self.lang.text_to_sequence(text, self.hp.data.text_cleaners)
        text = torch.LongTensor(text).unsqueeze(0).cuda()

        speaker = in_text.speaker
        speaker = torch.LongTensor([speaker]).cuda()

        return self.model.inference(text, speaker, self.max_decoder_steps, mel_chunk_size)

    def _gen_hparams(self, config_paths):
        # generate hparams object for pl.LightningModule
        parser = argparse.ArgumentParser()
        parser.add_argument('--config')
        args = parser.parse_args(['--config', config_paths])
        return args

    def _set_model(self, in_model):
        logging.debug('_set_model/in_model/%s', in_model)
        try:
            if not os.path.exists(in_model.path):
                raise RuntimeError("Model {} does not exist.".format(in_model.path))
            max_decoder_steps = int(in_model.config["max_decoder_steps"])
        except Exception as e:
            logging.exception(e)
            return tts_pb2.SetModelResult(result=False, error=str(e))

        try:
            self._load_checkpoint(in_model.path, max_decoder_steps)
            self._save_config(in_model.path, max_decoder_steps)
            return tts_pb2.SetModelResult(result=True)
        except Exception as e:
            logging.exception(e)
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            return tts_pb2.SetModelResult(result=False, error=str(e))

    def _load_checkpoint(self, checkpoint_path, max_decoder_steps):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        self.max_decoder_steps = max_decoder_steps
        self.model_status.model.config["max_decoder_steps"] = str(max_decoder_steps)

        if checkpoint_path != self.model_status.model.path:
            self.model_status.model.path = checkpoint_path
            checkpoint = torch.load(checkpoint_path, map_location='cuda:0')

            if not hasattr(self, 'model') \
                    or self.hp.audio != checkpoint['hp'].audio \
                    or self.hp.chn != checkpoint['hp'].chn \
                    or self.hp.ker != checkpoint['hp'].ker \
                    or self.hp.depth != checkpoint['hp'].depth:
                args = self._gen_hparams(checkpoint['config_path'])
                self.model = Tacotron(args, checkpoint['hp']).cuda()
                self.hp = self.model.hp
                self.lang = Language(self.hp.data.lang, self.hp.data.text_cleaners)
                self.model.eval()
                self.model.freeze()

                if self.is_fp16:
                    self.model.teacher.attention_layer.score_mask_value = np.finfo('float16').min
                    from apex import amp
                    self.model.encoder, _ = amp.initialize(self.model.encoder, [], opt_level="O3")
                    self.model.speaker_embedding, _ = amp.initialize(self.model.speaker_embedding, [], opt_level="O3")
                    self.model.teacher, _ = amp.initialize(self.model.teacher, [], opt_level="O3")

            if self.hp.data.lang != checkpoint['hp'].data.lang \
                    or self.hp.data.text_cleaners != checkpoint['hp'].data.text_cleaners:
                self.model.hp.data.lang = checkpoint['hp'].data.lang
                self.model.hp.data.text_cleaners = checkpoint['hp'].data.text_cleaners

                self.lang = Language(self.hp.data.lang, self.hp.data.text_cleaners)

                self.model.symbols = self.lang.get_symbols()
                self.model.symbols = ['"{}"'.format(symbol) for symbol in self.model.symbols]

                self.model.encoder.embedding = torch.nn.Embedding(
                    len(self.model.symbols), self.hp.chn.encoder)
                self.model.encoder.embedding.cuda()
                self.model.encoder.embedding.eval()
                if self.is_fp16:
                    self.model.encoder.embedding.weight.data = self.model.encoder.embedding.weight.data.half()

            if len(self.hp.data.speakers) != len(checkpoint['hp'].data.speakers):
                self.model.speaker_embedding = torch.nn.Embedding(
                    len(checkpoint['hp'].data.speakers), self.hp.chn.speaker)
                self.model.speaker_embedding.cuda()
                self.model.speaker_embedding.eval()
                if self.is_fp16:
                    self.model.speaker_embedding.weight.data = self.model.speaker_embedding.weight.data.half()

            self.model.hp.data.speakers = checkpoint['hp'].data.speakers

            self.model.load_state_dict(checkpoint['state_dict'])

            if self.hp.audio.mel_fmax is None:
                self.hp.audio.mel_fmax = self.model.hp.audio.mel_fmax = \
                    self.hp.audio.sampling_rate // 2
            self.mel_config = tts_pb2.MelConfig(
                filter_length=self.hp.audio.filter_length,
                hop_length=self.hp.audio.hop_length,
                win_length=self.hp.audio.win_length,
                n_mel_channels=self.hp.audio.n_mel_channels,
                sampling_rate=self.hp.audio.sampling_rate,
                mel_fmin=self.hp.audio.mel_fmin,
                mel_fmax=self.hp.audio.mel_fmax,
            )
            self.max_speaker = len(self.hp.data.speakers) - 1

        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING

    def _save_config(self, checkpoint_path, max_decoder_steps):
        if self.backup_path != "":
            config = OmegaConf.create({
                'path': checkpoint_path,
                'max_decoder_steps': max_decoder_steps
            })
            OmegaConf.save(config, self.backup_path)
            logging.debug('Save Config File: {}'.format(self.backup_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='tacotron(DCA) runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=30001)
    parser.add_argument('-s', '--max_decoder_steps',
                        nargs='?',
                        dest='max_decoder_steps',
                        help='voice length limit: (max decoder steps * hop_length / sampling_rate) second',
                        type=int,
                        default=30000)
    parser.add_argument('-w', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='max workers',
                        type=int,
                        default=4)
    parser.add_argument('--mel_chunk_size',
                        nargs='?',
                        dest='mel_chunk_size',
                        help='mel chunk size',
                        type=int,
                        default=88)
    parser.add_argument('--is_fp16', action='store_true',
                        help='fp16 mode')
    parser.add_argument('-b', '--backup_config', type=str, default="",
                        help='yaml file for backup configuration')

    args = parser.parse_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    tacotron = TacotronImpl(
        args.model, args.max_decoder_steps, args.mel_chunk_size,
        args.is_fp16, args.backup_config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers),)
    add_AcousticServicer_to_server(tacotron, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logging.info('tacotron(DCA) starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
